FROM openjdk:8
LABEL maintener "gconte"
ADD /target/cadastroClienteAPI-1.0-SNAPSHOT.jar .
ENTRYPOINT ["java" , "-jar" , "cadastroClienteAPI-1.0-SNAPSHOT.jar"]