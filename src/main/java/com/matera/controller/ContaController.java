package com.matera.controller;

import com.matera.domain.entity.Conta;
import com.matera.service.ContaService;
import java.math.BigInteger;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clientes/contas")
@Validated
public class ContaController {

  @Autowired
  private ContaService contaService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Conta save(@RequestBody @Valid Conta clienteConta){
    return contaService.save(clienteConta);
  }

  @PutMapping("/{codigo_cliente}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void update(@PathVariable BigInteger codigo_cliente,
      @RequestParam(name = "conta") @NotBlank String conta,
      @RequestBody @Valid Conta clienteConta){
    contaService.update(codigo_cliente,conta,clienteConta);
  }

  @DeleteMapping("/{codigo_cliente}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable BigInteger codigo_cliente,
      @RequestParam(name = "conta") @NotBlank String conta){
    contaService.delete(codigo_cliente,conta);
  }

  @GetMapping("/{codigo_cliente}")
  @ResponseStatus(HttpStatus.OK)
  public List<Conta> find(@PathVariable BigInteger codigo_cliente){
    return contaService.find(codigo_cliente);
  }
}
