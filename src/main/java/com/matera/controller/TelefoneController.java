package com.matera.controller;

import com.matera.domain.entity.Telefone;
import com.matera.service.TelefoneService;
import java.math.BigInteger;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clientes/telefones")
@Validated
public class TelefoneController {

    @Autowired
    private TelefoneService telefoneService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Telefone save(@RequestBody @Valid Telefone clienteTelefone){
        return telefoneService.save(clienteTelefone);
    }

    @PutMapping("/{codigo_cliente}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable BigInteger codigo_cliente,
        @RequestParam(name = "telefone") @NotBlank String telefone,
        @RequestBody Telefone clienteTelefone){
        telefoneService.update(codigo_cliente,telefone,clienteTelefone);
    }

    @DeleteMapping("/{codigo_cliente}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable BigInteger codigo_cliente,
        @RequestParam(name = "telefone") @NotBlank String telefone){
        telefoneService.delete(codigo_cliente,telefone);
    }

    @GetMapping("{codigo_cliente}")
    @ResponseStatus(HttpStatus.OK)
    public List<Telefone> find(@PathVariable BigInteger codigo_cliente){
        return telefoneService.find(codigo_cliente);
    }
}