package com.matera.controller;

import com.matera.ApiErrors;
import com.matera.exception.ClienteNotFoundException;
import com.matera.exception.ContaNotFoundException;
import com.matera.exception.TelefoneNotFoundException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleRegraNegocioException(MethodArgumentNotValidException ex){
        List<String> errors = ex.getBindingResult()
            .getAllErrors()
            .stream()
            .map( error -> error.getDefaultMessage())
            .collect(Collectors.toList());

        return new ApiErrors(errors);
    }

    @ExceptionHandler(ClienteNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleClienteNotFoundException(ClienteNotFoundException ex){
        return new ApiErrors(ex.getMessage());
    }

    @ExceptionHandler(TelefoneNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleTelefoneNotFoundException(TelefoneNotFoundException ex){
        return new ApiErrors(ex.getMessage());
    }

    @ExceptionHandler(ContaNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleContaNotFoundException(ContaNotFoundException ex){
        return new ApiErrors(ex.getMessage());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleMissingParameter(MissingServletRequestParameterException ex){
        return new ApiErrors("O parametro '"+ex.getParameterName()+"' e obrigatorio!");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleBlankParameter(ConstraintViolationException ex){
        return new ApiErrors("O parametro nao pode ser em branco!");
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleDuplicateValuesForUniqueKeys(DataIntegrityViolationException ex){
        return new ApiErrors("Os dados já estao cadastrados!");
    }
}

