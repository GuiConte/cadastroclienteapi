package com.matera.dto;

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "telefone")
public class TelefoneDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger cod_telefone;

    @ManyToOne
    @JoinColumn(name = "cod_cliente")
    private ClienteDTO cliente;

    @Column(name = "telefone", length = 30)
    private String telefone;

}
