package com.matera.dto;

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conta",
    uniqueConstraints=@UniqueConstraint(columnNames={"conta", "agencia"},name = "uk_conta_agencia"))
public class ContaDTO {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private BigInteger cod_conta;

  @ManyToOne
  @JoinColumn(name = "cod_cliente")
  private ClienteDTO cliente;

  @Column(name = "agencia", length = 20)
  private String agencia;

  @Column(name = "conta", length = 20)
  private String conta;

}
