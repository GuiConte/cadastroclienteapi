package com.matera.exception;

public class ContaNotFoundException extends RuntimeException{

  public ContaNotFoundException() {
    super("Conta nao encontrada !");
  }
}
