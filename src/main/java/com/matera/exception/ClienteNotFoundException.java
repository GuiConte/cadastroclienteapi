package com.matera.exception;

public class ClienteNotFoundException extends RuntimeException {

    public ClienteNotFoundException() {
        super("Cliente nao encontrado!");
    }

}
