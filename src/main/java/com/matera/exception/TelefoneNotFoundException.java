package com.matera.exception;

public class TelefoneNotFoundException extends RuntimeException{
    public TelefoneNotFoundException() {
        super("Telefone não encontrado !");
    }
}
