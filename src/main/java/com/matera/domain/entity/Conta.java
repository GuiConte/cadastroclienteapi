package com.matera.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigInteger;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Conta {

  @JsonIgnore
  private BigInteger codigo;

  @NotNull(message = "O campo 'cliente' e obrigatorio!")
  private BigInteger cliente;

  @NotEmpty(message = "O campo 'agencia' e obrigatorio!")
  private String agencia;

  @NotEmpty(message = "O campo 'conta' e obrigatorio!")
  private String conta;

}
