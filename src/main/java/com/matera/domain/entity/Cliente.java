package com.matera.domain.entity;

import java.math.BigInteger;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {

    private BigInteger codigo;

    @NotEmpty(message = "O campo 'nome' e obrigatorio!")
    private String nome;

    @NotEmpty(message = "O campo 'cpf' e obrigatorio!")
    @CPF(message = "O cpf informado e invalido !")
    private String cpf;

    @NotEmpty(message = "O campo 'endereco' e obrigatorio!")
    private String endereco;

    //private List<Telefone> telefones;

}
