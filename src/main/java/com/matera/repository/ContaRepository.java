package com.matera.repository;

import com.matera.dto.ContaDTO;
import com.matera.dto.ClienteDTO;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContaRepository extends JpaRepository<ContaDTO, BigInteger> {

  Optional<ContaDTO> findByClienteAndConta(ClienteDTO cliente, String conta);

  List<ContaDTO> findByCliente(ClienteDTO clienteDTO);

}
