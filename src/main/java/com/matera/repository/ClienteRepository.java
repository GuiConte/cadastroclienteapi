package com.matera.repository;

import com.matera.dto.ClienteDTO;
import java.math.BigInteger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<ClienteDTO, BigInteger> {
}
