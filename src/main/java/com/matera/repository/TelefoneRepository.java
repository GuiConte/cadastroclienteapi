package com.matera.repository;

import com.matera.dto.ClienteDTO;
import com.matera.dto.TelefoneDTO;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TelefoneRepository extends JpaRepository<TelefoneDTO, BigInteger> {

    Optional<TelefoneDTO> findByClienteAndTelefone(ClienteDTO cliente, String telefone);

    List<TelefoneDTO> findByCliente(ClienteDTO clienteDTO);
}
