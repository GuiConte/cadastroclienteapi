package com.matera.service.converter;

import com.matera.domain.entity.Conta;
import com.matera.dto.ContaDTO;
import com.matera.dto.ClienteDTO;
import java.util.ArrayList;
import java.util.List;

public class ContaConverter {

  public static ContaDTO toDTO(ClienteDTO clienteDTO, Conta clienteConta){
    return ContaDTO
        .builder()
        .cod_conta(clienteConta.getCodigo())
        .cliente(clienteDTO)
        .agencia(clienteConta.getAgencia())
        .conta(clienteConta.getConta())
        .build();
  }

  public static Conta toBusinessObject(ContaDTO contaDTO){
    return Conta
        .builder()
        .codigo(contaDTO.getCod_conta())
        .cliente(contaDTO.getCliente().getCod_cliente())
        .agencia(contaDTO.getAgencia())
        .conta(contaDTO.getConta())
        .build();
  }

  public static List<Conta> toBusinessObject(List<ContaDTO> clientesContaDTO){
    List<Conta> clientesConta = new ArrayList<Conta>();
    clientesContaDTO.forEach(
        conta ->{
          clientesConta.add(toBusinessObject(conta));
        }
    );
    return clientesConta;
  }

}
