package com.matera.service.converter;

import com.matera.domain.entity.Telefone;
import com.matera.dto.ClienteDTO;
import com.matera.dto.TelefoneDTO;
import java.util.ArrayList;
import java.util.List;

public class TelefoneConverter {

    public static TelefoneDTO toDTO(ClienteDTO clienteDTO, Telefone clienteTelefone){
        return TelefoneDTO.builder()
            .cod_telefone(clienteTelefone.getCodigo())
            .cliente(clienteDTO)
            .telefone(clienteTelefone.getTelefone())
            .build();
    }

    public static Telefone toBusinessObject(TelefoneDTO telefoneDTO){
        return Telefone.builder()
            .codigo(telefoneDTO.getCod_telefone())
            .cliente(telefoneDTO.getCliente().getCod_cliente())
            .telefone(telefoneDTO.getTelefone())
            .build();
    }

    public static List<Telefone> toBusinessObject(List<TelefoneDTO> clientesTelefoneDTO){
        List<Telefone> clientesTelefone = new ArrayList<Telefone>();
        clientesTelefoneDTO.forEach(
            telefones ->{
                clientesTelefone.add(toBusinessObject(telefones));
            }
        );
        return clientesTelefone;
    }

//    public static List<Telefone> toBusinessObject(List<TelefoneDTO> clienteTelefoneDTO){
//        return clienteTelefoneDTO
//            .stream()
//            .map(
//                telefoneDTO -> Telefone
//                    .builder()
//                    .telefone(telefoneDTO.getTelefone())
//                    .build()
//            ).collect(Collectors.toList());
//
//    }

}
