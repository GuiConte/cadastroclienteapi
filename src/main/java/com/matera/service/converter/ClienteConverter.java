package com.matera.service.converter;

import com.matera.domain.entity.Cliente;
import com.matera.dto.ClienteDTO;
import java.util.ArrayList;
import java.util.List;

public class ClienteConverter {
    public static ClienteDTO toDTO(Cliente cliente){
        return ClienteDTO.builder()
            .cod_cliente(cliente.getCodigo())
            .nome(cliente.getNome())
            .cpf(cliente.getCpf())
            .endereco(cliente.getEndereco())
            .build();
    }

    public static Cliente toBusinessObject(ClienteDTO clienteDTO){
        return Cliente.builder()
            .codigo(clienteDTO.getCod_cliente())
            .nome(clienteDTO.getNome())
            .cpf(clienteDTO.getCpf())
            .endereco(clienteDTO.getEndereco())
//            .telefones(TelefoneConverter.toBusinessObject(clienteDTO.getTelefones()))
            .build();
    }

    public static List<Cliente> toBusinessObject(List<ClienteDTO> clientesDTO){
        List<Cliente> clientes = new ArrayList<Cliente>();
        clientesDTO.forEach(
            clienteDTO ->{
                clientes.add(toBusinessObject(clienteDTO));
            }
        );
        return clientes;
    }
}
