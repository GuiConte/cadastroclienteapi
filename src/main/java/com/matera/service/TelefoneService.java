package com.matera.service;

import com.matera.domain.entity.Telefone;
import java.math.BigInteger;
import java.util.List;

public interface TelefoneService {

    Telefone save(Telefone clienteTelefone);

    void update(BigInteger codigo_cliente,String telefone,
        Telefone clienteTelefone);

    void delete(BigInteger codigo_cliente, String telefone);

    List<Telefone> find(BigInteger codigo_cliente);

}
