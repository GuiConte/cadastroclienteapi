package com.matera.service;

import com.matera.domain.entity.Conta;
import java.math.BigInteger;
import java.util.List;

public interface ContaService {

  Conta save(Conta clienteConta);

  void update(BigInteger codigo_cliente, String conta,
      Conta clienteConta);

  void delete(BigInteger codigo_cliente, String conta);

  List<Conta> find(BigInteger codigo_cliente);

}
