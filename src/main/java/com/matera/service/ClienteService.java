package com.matera.service;

import com.matera.domain.entity.Cliente;
import java.math.BigInteger;
import java.util.List;

public interface ClienteService {

    Cliente save(Cliente cliente);

    Cliente update(BigInteger codigo, Cliente cliente);

    void delete(BigInteger codigo);

    List<Cliente> find();
}
