package com.matera.service.impl;

import static com.matera.service.converter.ClienteConverter.toBusinessObject;
import static com.matera.service.converter.ClienteConverter.toDTO;

import com.matera.domain.entity.Cliente;
import com.matera.dto.ClienteDTO;
import com.matera.exception.ClienteNotFoundException;
import com.matera.repository.ClienteRepository;
import com.matera.service.ClienteService;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

  @Autowired
  private ClienteRepository clienteRepository;

  @Override
  public Cliente save(Cliente cliente) {
    ClienteDTO clienteDTO = clienteRepository.save(toDTO(cliente));
    return toBusinessObject(clienteDTO);
  }

  @Override
  public Cliente update(BigInteger codigo, Cliente cliente) {
    return clienteRepository
        .findById(codigo)
        .map(clienteExiste -> {
          cliente.setCodigo(clienteExiste.getCod_cliente());
          clienteRepository.save(toDTO(cliente));
          return cliente;
        })
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  public void delete(BigInteger codigo) {
    clienteRepository
        .findById(codigo)
        .map(clienteExiste -> {
          clienteRepository.delete(clienteExiste);
          return clienteExiste;
        })
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  public List<Cliente> find() {
    List<ClienteDTO> clientesDTO = clienteRepository.findAll();
    return toBusinessObject(clientesDTO);
  }
}
