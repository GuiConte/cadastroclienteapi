package com.matera.service.impl;

import static com.matera.service.converter.TelefoneConverter.toBusinessObject;
import static com.matera.service.converter.TelefoneConverter.toDTO;

import com.matera.domain.entity.Telefone;
import com.matera.dto.TelefoneDTO;
import com.matera.exception.ClienteNotFoundException;
import com.matera.exception.TelefoneNotFoundException;
import com.matera.repository.ClienteRepository;
import com.matera.repository.TelefoneRepository;
import com.matera.service.TelefoneService;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TelefoneServiceImpl implements TelefoneService {

  @Autowired
  private ClienteRepository clienteRepository;

  @Autowired
  private TelefoneRepository telefoneRepository;

  @Override
  @Transactional
  public Telefone save(Telefone clienteTelefone) {
    return clienteRepository
        .findById(clienteTelefone.getCliente())
        .map(
            clienteExiste -> {
              TelefoneDTO telefoneDTO =
                  telefoneRepository.save(toDTO(clienteExiste,clienteTelefone));
              return toBusinessObject(telefoneDTO);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  @Transactional
  public void update(BigInteger codigo_cliente,
      String telefone, Telefone clienteTelefone) {
    clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              return telefoneRepository
                  .findByClienteAndTelefone(clienteExiste, telefone)
                  .map(telefoneExiste -> {
                    telefoneExiste.setTelefone(clienteTelefone.getTelefone());
                    telefoneRepository.save(telefoneExiste);
                    return telefoneExiste;
                  })
                  .orElseThrow(TelefoneNotFoundException::new);
            })
        .orElseThrow(ClienteNotFoundException::new);

  }

  @Override
  @Transactional
  public void delete(BigInteger codigo_cliente, String telefone) {
    clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              return telefoneRepository
                  .findByClienteAndTelefone(clienteExiste, telefone)
                  .map(telefoneExiste -> {
                    telefoneRepository.delete(telefoneExiste);
                    return telefoneExiste;
                  })
                  .orElseThrow(TelefoneNotFoundException::new);
            })
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  @Transactional
  public List<Telefone> find(BigInteger codigo_cliente) {
    return clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              List<TelefoneDTO> clientesTelefoneDTO =
                  telefoneRepository.findByCliente(clienteExiste);
              return toBusinessObject(clientesTelefoneDTO);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }
}
