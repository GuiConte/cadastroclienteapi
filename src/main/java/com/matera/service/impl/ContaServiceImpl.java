package com.matera.service.impl;

import static com.matera.service.converter.ContaConverter.toBusinessObject;
import static com.matera.service.converter.ContaConverter.toDTO;

import com.matera.domain.entity.Conta;
import com.matera.dto.ContaDTO;
import com.matera.exception.ClienteNotFoundException;
import com.matera.exception.ContaNotFoundException;
import com.matera.repository.ContaRepository;
import com.matera.repository.ClienteRepository;
import com.matera.service.ContaService;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContaServiceImpl implements ContaService {

  @Autowired
  private ClienteRepository clienteRepository;

  @Autowired
  private ContaRepository contaRepository;

  @Override
  @Transactional
  public Conta save(Conta clienteConta) {
    return clienteRepository
        .findById(clienteConta.getCliente())
        .map(
            clienteExiste -> {
              ContaDTO contaDTO =
                  contaRepository.save(
                      toDTO(clienteExiste,clienteConta));
              return toBusinessObject(contaDTO);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  @Transactional
  public void update(BigInteger codigo_cliente, String conta, Conta clienteConta) {
    clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              return contaRepository
                  .findByClienteAndConta(clienteExiste,conta)
                  .map( contaExiste -> {
                    contaExiste.setConta(clienteConta.getConta());
                    contaExiste.setAgencia(clienteConta.getAgencia());
                    contaRepository.save(contaExiste);
                    return contaExiste;
                  })
                  .orElseThrow(ContaNotFoundException::new);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  @Transactional
  public void delete(BigInteger codigo_cliente, String conta) {
    clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              return contaRepository
                  .findByClienteAndConta(clienteExiste,conta)
                  .map( contaExiste -> {
                    contaRepository.delete(contaExiste);
                    return contaExiste;
                  })
                  .orElseThrow(ContaNotFoundException::new);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }

  @Override
  @Transactional
  public List<Conta> find(BigInteger codigo_cliente) {
    return clienteRepository
        .findById(codigo_cliente)
        .map(
            clienteExiste -> {
              List<ContaDTO> contaDTO =
                  contaRepository.findByCliente(clienteExiste);
              return toBusinessObject(contaDTO);
            }
        )
        .orElseThrow(ClienteNotFoundException::new);
  }
}
