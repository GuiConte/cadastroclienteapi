USE cadastroClienteAPI;

CREATE TABLE cliente(
    cod_cliente BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100),
    cpf VARCHAR(11),
    endereco VARCHAR(100)
);

CREATE TABLE cliente_telefone(
    cod_cliente_telefone BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cod_cliente BIGINT,
    telefone VARCHAR(30),
    CONSTRAINT fk_cliente_telefone
        FOREIGN KEY (cod_cliente)
        REFERENCES cliente(cod_cliente)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE cliente_conta(
    cod_cliente_conta BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cod_cliente BIGINT,
    agencia VARCHAR(20),
    conta VARCHAR(20),
    CONSTRAINT fk_cliente_conta
        FOREIGN KEY (cod_cliente)
        REFERENCES cliente(cod_cliente)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);